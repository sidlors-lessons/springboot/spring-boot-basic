package com.example.demo.controller;
import java.util.concurrent.atomic.AtomicLong;

import com.example.demo.model.Greeting;
import com.example.demo.respository.GreetingRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	@Autowired
	GreetingRepository repository;

	@GetMapping("/greeting")
	public ResponseEntity<Greeting> greeting(@RequestParam(value = "id", defaultValue = "1") long id) {
		Greeting findedGreeting=repository.findById(id);
		if(findedGreeting == null){
			return new ResponseEntity<Greeting>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Greeting>(findedGreeting, HttpStatus.OK);
	}

	@PostMapping("/greeting")
	public  void greeting(@RequestBody Greeting greeting) {
		repository.save(greeting);
	}
	

}