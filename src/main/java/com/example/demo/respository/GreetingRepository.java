package com.example.demo.respository;

import java.util.List;

import com.example.demo.model.Greeting;
import org.springframework.data.repository.CrudRepository;

public interface  GreetingRepository  extends CrudRepository<Greeting, Long> {

    List<Greeting> findByContent(String content);
    Greeting findById(long id);
    
  }